
const
    request = require('request'),
    config = require('../config/config');

// return the list of Job Post by Location name
function getJobPostsByLocationName(req, res) {
    let url = `${config.development.base_url}?location=${req.query.locName}`;

    function callback(error, response, body) {
        if (!error && response.statusCode == 200) {
            var info = JSON.parse(body);
            res.send(info);
        }
    }
    request.get({ url: url }, callback);
}

// return the list of Job Post by description // was not used
function getJobPostsByDescription(req, res) {
    let url = `${config.development.base_url}?location=${req.query.locName}`;

    function callback(error, response, body) {
        if (!error && response.statusCode == 200) {
            var info = JSON.parse(body);
            res.send(info);
        }
    }
    request.get({ url: url }, callback);
}

module.exports = {
    getJobPostsByLocationName,
    getJobPostsByDescription
};