const
    express = require("express"),
    bodyParser = require("body-parser"),
    jobPost = require('./routes/JobPostRoute'),
    cors = require('cors'),
    config = require('./config/config'),
    app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/jobPosts', jobPost);

// start the server
app.listen(config.development.node_port, () => {
    console.log(`Server run on the port nummer: ${config.development.node_port}`);
});
