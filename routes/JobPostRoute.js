const
    jobPostController = require("../controllers/JobPostController.js"),
    express = require('express');

const router = express.Router();

// Define the Routes
router.get('/location', jobPostController.getJobPostsByLocationName);
router.get('/description', jobPostController.getJobPostsByDescription);

module.exports = router;